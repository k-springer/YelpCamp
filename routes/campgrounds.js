const express = require('express');
const router = express.Router();
const Campground = require('../models/campground');
const middleware = require('../middleware');

// Index
router.get('/', function (req, res) {
    // Get all campgrounds from DB
    Campground.find({}, function(err, allCampgrounds) {
        if (err) {
            console.log(err);
        } else {
            res.render('campgrounds/index', {campgrounds: allCampgrounds});
        }
    });
});

// Create Campground
router.post('/', middleware.isLoggedIn, function(req, res) {
    // Get data from form
    const name = req.body.name;
    const price = req.body.price;
    const image = req.body.image;
    const desc = req.body.description;
    const author = {
        id: req.user._id,
        username: req.user.username
    };
    
    Campground.create({
        name: name,
        price: price,
        image: image,
        description: desc,
        author: author
    }, function (err, campground) {
        if (err) {
            console.log(err);
        } else {
            console.log(campground);
            res.redirect('/campgrounds');
        }
    });
});

// New Campground
router.get('/new', middleware.isLoggedIn, function (req, res) {
    res.render("campgrounds/new")
});

// Show Campground
router.get('/:id', function (req, res) {
    Campground.findById(req.params.id).populate('comments').exec(function(err, foundCampground) {
        if (err) {
            console.log(err);
        } else {
            res.render('campgrounds/show', {campground: foundCampground});
        }
    });
});

// Edit Campground
router.get('/:id/edit', middleware.checkCampgroundOwnership, function (req, res) {
    Campground.findById(req.params.id, function(err, foundCampground) {
        if(err) {
            req.flash('error', 'Campground was not found');
            res.redirect('back');
        }
        res.render("campgrounds/edit", {campground: foundCampground});
    });
});

// Update Campground
router.put('/:id/edit', middleware.checkCampgroundOwnership, function (req, res) {
    Campground.findByIdAndUpdate(req.params.id, req.body.campground, function(err, updatedCampground) {
        if (err) {
            res.redirect('/campgrounds');
        } else {
            res.redirect('/campgrounds/' + req.params.id);
        }
    });
});

// Delete Campground
router.delete('/:id', middleware.checkCampgroundOwnership, function (req, res) {
    Campground.findByIdAndRemove(req.params.id, function(err) {
        if (err) {
            res.redirect('/campgrounds');
        } else {
            res.redirect('/campgrounds');
        }
    });
});

module.exports = router;